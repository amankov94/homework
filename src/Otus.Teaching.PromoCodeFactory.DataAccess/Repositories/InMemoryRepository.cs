﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new List<T>(data);
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task DeleteAsync(T entity)
        {
            return Task.Run(() => Data.Remove(entity));
        }

        public Task UpdateAsync(T entity)
        {
            return Task.Run(() => Data[Data.IndexOf(entity)] = entity);
        }

        public Task CreateAsync(T entity)
        {
            return Task.Run(() => Data.Add(entity));
        }
    }
}